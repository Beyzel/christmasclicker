﻿using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class AdvertisementsController : MonoBehaviour
    {
        private readonly string _androidGameId = "1630172";
        private readonly string _iosGameId = "1630173";

        [SerializeField] private GameController _gameController;
        [SerializeField] private PurchaseManager _purchaseManager;
        [SerializeField] private GameObject _ads;
        [SerializeField] private Sprite _adsFreeSprite;

        private bool _restart;
        private readonly PlayerPrefsController _playerPrefsController = new PlayerPrefsController();

        private void Awake()
        {
           
        }

        private void OnEnable()
        {
            PurchaseManager.OnPurchaseNonConsumable += PurchaseManagerOnOnPurchaseNonConsumable;
        }

        private void OnDisable()
        {
            PurchaseManager.OnPurchaseNonConsumable -= PurchaseManagerOnOnPurchaseNonConsumable;
        }

        private void PurchaseManagerOnOnPurchaseNonConsumable(PurchaseEventArgs args)
        {
            _playerPrefsController.SetString("IsAdsBuy", "true");
            ChangeAdsBtnCprite();
            Debug.Log("You purchase: " + args.purchasedProduct.definition.id);
        }

        private void Start()
        {
            var gameId = string.Empty;
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                {
                    gameId = _androidGameId;
                    break;
                }

                case RuntimePlatform.IPhonePlayer:
                {
                    gameId = _iosGameId;
                    break;
                }

                case RuntimePlatform.WindowsEditor:
                {
                    gameId = _androidGameId;
                    break;
                }
            }

            Advertisement.Initialize(gameId);
        }

        public void ShowAds(bool restart = false)
        {
            _restart = restart;
            if (Advertisement.IsReady())
            {
                var options = new ShowOptions
                {
                    resultCallback = HandleShowResult
                };

                Advertisement.Show(options);
            }
        }

        private void HandleShowResult(ShowResult result)
        {
            if (result == ShowResult.Finished)
            {
                GetAdvertisementRegard();
                Debug.Log("Finish");
            }
            else if (result == ShowResult.Skipped)
            {
                GetAdvertisementRegard();
                Debug.Log("Skiped");
            }
            else if (result == ShowResult.Failed)
            {
                if (_restart)
                {
                    _gameController.StartGame();
                }
                Debug.Log("Failed");
            }
        }

        private void GetAdvertisementRegard()
        {
            if (!_restart)
            {
                _gameController.AddHealthForWatchingAds();
            }
            else
            {
                _gameController.StartGame();
            }
        }

        public void OnAdsBtnClick()
        {
            _purchaseManager.BuyNonConsumable(0);
        }

        public void ChangeAdsBtnCprite()
        {
            _ads.GetComponent<Button>().enabled = false;
            _ads.GetComponent<Image>().sprite = _adsFreeSprite;
        }
    }
}