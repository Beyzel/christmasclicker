﻿using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerPrefsController 
    {
        public bool HasKey(string keyName)
        {
            return PlayerPrefs.HasKey (keyName);
        }

        public void SetString(string keyName, string value)
        {
            PlayerPrefs.SetString (keyName, value);
            PlayerPrefs.Save ();
        }

        public void SetInt(string keyName, int value)
        {
            PlayerPrefs.SetInt (keyName, value);
            PlayerPrefs.Save ();
        }

        public void SetFloat(string keyName, float value)
        {
            PlayerPrefs.SetFloat (keyName, value);
            PlayerPrefs.Save ();
        }

        public int GetInt(string keyName)
        {
            var value = PlayerPrefs.GetInt (keyName);
            return value;
        }

        public string GetString(string keyName)
        {
            var value = PlayerPrefs.GetString (keyName);
            return value;
        }

        public float GetFloat(string keyName)
        {
            var value = PlayerPrefs.GetFloat (keyName);
            return value;
        }

        public void Delete(string keyName)
        {
            PlayerPrefs.DeleteKey (keyName);
            PlayerPrefs.Save ();
        }

        public void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}