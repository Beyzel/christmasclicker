﻿using UnityEngine;

namespace Assets.Scripts
{
    public class SettingsController : MonoBehaviour
    {
        [SerializeField] private GameObject _settings;
        [SerializeField] private BagController _bagController;

        public void OnSettingsBtnClick()
        {
            _bagController.ChangeWindow(Windows.Settings);
        }
    }
}