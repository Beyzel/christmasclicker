﻿using UnityEngine;

namespace Assets.Scripts
{
    public class MoveSnow : MonoBehaviour
    {
        [SerializeField] private RectTransform _firstSnow;
        [SerializeField] private RectTransform _secondSnow;
        [SerializeField] private float _step;

        private float _snowHeight;

        private void Start()
        {
            _snowHeight = _firstSnow.rect.height;
        }

        private void Update()
        {
            SnowMoving();
        }

        private void SnowMoving()
        {
            _firstSnow.anchoredPosition = new Vector2(_firstSnow.anchoredPosition.x, _firstSnow.anchoredPosition.y - _step);
            _secondSnow.anchoredPosition = new Vector2(_secondSnow.anchoredPosition.x, _secondSnow.anchoredPosition.y - _step);

            if (_firstSnow.anchoredPosition.y < -_snowHeight)
            {
                _firstSnow.anchoredPosition = new Vector2(0, _snowHeight);
            }

            if (_secondSnow.anchoredPosition.y < -_snowHeight)
            {
                _secondSnow.anchoredPosition = new Vector2(0, _snowHeight);
            }
        }
    }
}