﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Share
{
    public class ShareController : MonoBehaviour
    {
//        public SocialSharing SocialSharing;
        public int Score;

        public void OnShareBtnClick()
        {
            GetComponent<SocialSharing>().ShareScreenshot("I just scored " + Score + " points in #ChristmasClicker by #BODIgames ! Can you beat me?");
        }
    }
}