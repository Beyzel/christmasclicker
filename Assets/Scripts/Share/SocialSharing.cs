﻿using System;
using System.Collections;
using System.IO;
using IndigoBunting.SocialSharing;
using UnityEngine;

namespace Assets.Scripts.Share
{
    public class SocialSharing : MonoBehaviour
    {
        private string _tmpCurrentShareText;

        private void OnEnable()
        {
#if UNITY_ANDROID
            OnAndroidScreenshotSaved += SocialSharing_OnAndroidScreenshotSaved;
#endif
#if UNITY_IOS
            IOSPhotoGalleryHandler.OnImageSaved += IOSPhotoGalleryHandler_OnImageFinishedSaving;
            #endif
        }

        private void OnDisable()
        {
#if UNITY_ANDROID
            OnAndroidScreenshotSaved -= SocialSharing_OnAndroidScreenshotSaved;
#endif
#if UNITY_IOS
            IOSPhotoGalleryHandler.OnImageSaved -= IOSPhotoGalleryHandler_OnImageFinishedSaving;
            #endif
        }

        public void ShareScreenshot(string shareText)
        {
            _tmpCurrentShareText = shareText;

            StartCoroutine(CreateScreenshot());
        }

        public void ShareTexture2D(string shareText, Texture2D texture2D)
        {
            _tmpCurrentShareText = shareText;

            var filename = DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
            var pathToImage = Path.Combine(Application.persistentDataPath, filename + ".png");
            var dataToSave = texture2D.EncodeToPNG();
            File.WriteAllBytes(pathToImage, dataToSave);

#if UNITY_ANDROID
            if (OnAndroidScreenshotSaved != null)
            {
                OnAndroidScreenshotSaved(pathToImage);
            }
#endif

#if UNITY_IOS
            StartCoroutine(IOSPhotoGalleryHandler.SaveExisting(pathToImage));
            #endif
        }

        private IEnumerator CreateScreenshot()
        {
            yield return new WaitForEndOfFrame();
            var screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
            screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);
            screenTexture.Apply();

            var filename = DateTime.Now.ToString("yyyy-MM-dd-HHmmss");
            var pathToImage = Path.Combine(Application.persistentDataPath, filename + ".png");
            var dataToSave = screenTexture.EncodeToPNG();
            File.WriteAllBytes(pathToImage, dataToSave);

#if UNITY_ANDROID
            if (OnAndroidScreenshotSaved != null)
            {
                OnAndroidScreenshotSaved(pathToImage);
            }
#endif

#if UNITY_IOS
            StartCoroutine(IOSPhotoGalleryHandler.SaveExisting(pathToImage));
            #endif
        }

        private void NativeShare(string text, string pathToImage)
        {
#if UNITY_ANDROID
            var intentClass = new AndroidJavaClass("android.content.Intent");
            var intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            var uriClass = new AndroidJavaClass("android.net.Uri");
            var uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + pathToImage);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), text);
            intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
            var unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            currentActivity.Call("startActivity", intentObject);
#endif

#if UNITY_IOS
            GeneralSharingiOSBridge.ShareTextWithImage(pathToImage, text);
            #endif
        }

        private void IOSPhotoGalleryHandler_OnImageFinishedSaving(string pathToImage)
        {
            NativeShare(_tmpCurrentShareText, pathToImage);
        }

        private void SocialSharing_OnAndroidScreenshotSaved(string pathToImage)
        {
            NativeShare(_tmpCurrentShareText, pathToImage);
        }

#if UNITY_ANDROID
        public event Action<string> OnAndroidScreenshotSaved;
#endif
    }
}