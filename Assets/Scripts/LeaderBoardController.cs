﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class LeaderBoardController : MonoBehaviour
    {
        private const string Leaderboard = "CgkIseG4lagDEAIQAQ";
        public static LeaderBoardController Instance { get; private set; }

        private void Start()
        {
            Instance = this;
            PlayGamesClientConfiguration configuration = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(configuration);
            PlayGamesPlatform.Activate();
            PlayGamesPlatform.DebugLogEnabled = true;
            Social.localUser.Authenticate(success =>
            {
                if (success)
                {
                    Debug.Log("Success Authenticate");
                }
            });
        }

        public void SetScoreToLeaderboard(long score)
        {
            Social.ReportScore(score, Leaderboard, success =>
            {
                if (success)
                {
                    Debug.Log("Success added");
                }
            });
        }

        public void OnLeaderboardBtnClick()
        {
            Social.ShowLeaderboardUI();
        }
    }
}