﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class PurchaseManager : MonoBehaviour, IStoreListener
    {
        public delegate void OnFailedPurchase(Product product, PurchaseFailureReason failureReason);

        public delegate void OnSuccessConsumable(PurchaseEventArgs args);

        public delegate void OnSuccessNonConsumable(PurchaseEventArgs args);

        private static IStoreController _mStoreController;
        private static IExtensionProvider _mStoreExtensionProvider;
        private readonly PlayerPrefsController _playerPrefsController = new PlayerPrefsController();

        public string[] NcProducts;
        public string[] CProducts;

        private int _currentProductIndex;
        private bool _restoreResult;
        [SerializeField] private AdvertisementsController _advertisementsController;

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _mStoreController = controller;
            _mStoreExtensionProvider = extensions;
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (CProducts.Length > 0 &&
                String.Equals(args.purchasedProduct.definition.id, CProducts[_currentProductIndex],
                    StringComparison.Ordinal))
            {
                OnSuccessC(args);
            }

            else if (NcProducts.Length > 0 &&
                     String.Equals(args.purchasedProduct.definition.id, NcProducts[_currentProductIndex],
                         StringComparison.Ordinal))
            {
                OnSuccessNc(args);
            }

            else
            {
                Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'",
                    args.purchasedProduct.definition.id));
            }

            return PurchaseProcessingResult.Complete;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            OnFailedP(product, failureReason);
        }

        public static event OnSuccessConsumable OnPurchaseConsumable;
        public static event OnSuccessNonConsumable OnPurchaseNonConsumable;
        public static event OnFailedPurchase PurchaseFailed;

        private void Awake()
        {
            if (!_playerPrefsController.HasKey("IsAdsBuy"))
            {
                _playerPrefsController.SetString("IsAdsBuy", "false");
            }
            InitializePurchasing();
            StartCoroutine(WaitingForInitializing());
        }

        private IEnumerator WaitingForInitializing()
        {
            if (!IsInitialized())
            {
                yield return new WaitForSeconds(0.1f);
                StartCoroutine(WaitingForInitializing());
            }
            else
            {
                RestorePurchases();
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    StartCoroutine(WaitinForCheckBuing());
                }
                else
                {
                    CheckBuying();
//                    _playerPrefsController.SetString("IsAdsBuy", "true");
                }
            }
            
        }

        private IEnumerator WaitinForCheckBuing()
        {
            if (!_restoreResult)
            {
                yield return new WaitForSeconds(0.1f);
                StartCoroutine(WaitinForCheckBuing());
            }
            else
            {
                CheckBuying();
//                _playerPrefsController.SetString("IsAdsBuy", "true");
            }
        }

        private void CheckBuying()
        {
            if (CheckBuyState(NcProducts[0]) || bool.Parse(_playerPrefsController.GetString("IsAdsBuy")))
            {
                _playerPrefsController.SetString("IsAdsBuy", "true");
                _advertisementsController.ChangeAdsBtnCprite();
            }
            else
            {
                _playerPrefsController.SetString("IsAdsBuy", "false");
            }
        }

        public static bool CheckBuyState(string id)
        {
            Product product = _mStoreController.products.WithID(id);
            if (product.hasReceipt)
            {
                return true;
            }
            return false;
        }

        public void InitializePurchasing()
        {
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            foreach (string s in CProducts) builder.AddProduct(s, ProductType.Consumable);
            foreach (string s in NcProducts) builder.AddProduct(s, ProductType.NonConsumable);
            UnityPurchasing.Initialize(this, builder);
        }

        public bool IsInitialized()
        {
            return _mStoreController != null && _mStoreExtensionProvider != null;
        }

        public void BuyConsumable(int index)
        {
            _currentProductIndex = index;
            BuyProductId(CProducts[index]);
        }

        public void BuyNonConsumable(int index)
        {
            _currentProductIndex = index;
            BuyProductId(NcProducts[index]);
        }

        private void BuyProductId(string productId)
        {
            if (IsInitialized())
            {
                Product product = _mStoreController.products.WithID(productId);

                if (product != null && product.availableToPurchase)
                {
                    Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                    _mStoreController.InitiatePurchase(product);
                }
                else
                {
                    Debug.Log(
                        "BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                    OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
                }
            }
        }

        protected virtual void OnSuccessC(PurchaseEventArgs args)
        {
            if (OnPurchaseConsumable != null)
            {
                OnPurchaseConsumable(args);
            }
            Debug.Log(CProducts[_currentProductIndex] + " Buyed!");
        }

        protected virtual void OnSuccessNc(PurchaseEventArgs args)
        {
            if (OnPurchaseNonConsumable != null)
            {
                OnPurchaseNonConsumable(args);
            }
            Debug.Log(NcProducts[_currentProductIndex] + " Buyed!");
        }

        protected virtual void OnFailedP(Product product, PurchaseFailureReason failureReason)
        {
            if (PurchaseFailed != null)
            {
                PurchaseFailed(product, failureReason);
            }
            Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
                product.definition.storeSpecificId, failureReason));
        }

        public void RestorePurchases()
        {
            if (!IsInitialized())
            {
                Debug.Log("RestorePurchases FAIL. Not initialized.");
                return;
            }

            Debug.Log("RestorePurchases started ...");

            var apple = _mStoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions(result =>
            {
                _restoreResult = result;
                Debug.Log("RestorePurchases continuing: " + result +
                          ". If no further messages, no purchases available to restore.");
            });
        }
    }
}