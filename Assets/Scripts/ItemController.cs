﻿using System.Collections;
using UnityEngine;
using UnityEngine.iOS;

namespace Assets.Scripts
{
    public class ItemController : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;
        
        private RectTransform _rectTransform;
        private GameController _gameController;
        
        public float Step;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
            _gameController = FindObjectOfType<GameController>();
            _canvas = FindObjectOfType<Canvas>();
        }

        public void OnItemBtnClick()
        {
            _gameController.UpdateScore();
            Destroy(gameObject);
        }

        public void StartItemMoving()
        {
            StartCoroutine(ItemMovement());
        }

        private IEnumerator ItemMovement()
        {
            while (CalculateYItemPos() > 0)
            {
                CalculateYItemPos();
                _rectTransform.anchoredPosition = new Vector2(_rectTransform.anchoredPosition.x, _rectTransform.anchoredPosition.y - Step);
                yield return new WaitForSeconds(0.01f);
            }

            if (CalculateYItemPos() <= 0)
            {
                _gameController.UpdateScore(true);
                Destroy(gameObject);
            }   
        }

        private float CalculateYItemPos()
        {
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_canvas.transform as RectTransform, GetComponent<RectTransform>().anchoredPosition, _canvas.worldCamera, out pos);
            var startPosition = _canvas.transform.TransformPoint(pos);
            return Camera.main.WorldToViewportPoint(startPosition).y;
        }
    }
}