﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public enum Windows
    {
        None,
        Restart,
        Settings,
        Bonus
    }

    public class BagController : MonoBehaviour
    {
        public void ChangeWindow(Windows windows)
        {
            DisableWindows();
            transform.Find(windows.ToString()).gameObject.SetActive(true);
        }

        public void DisableWindows()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }
        }

    }
}