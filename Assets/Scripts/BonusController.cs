﻿using System;
using System.Collections;
using System.Collections.Generic;
using OneSignalPush.MiniJSON;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Assets.Scripts
{
    public class BonusController : MonoBehaviour
    {
        private const int DayInSeconds = 86399;
        private readonly PlayerPrefsController _playerPrefsController = new PlayerPrefsController();
       
        [SerializeField] private List<Sprite> _bonuSpritesList = new List<Sprite>();
        [SerializeField] private List<Sprite> _gamebonuSpritesList = new List<Sprite>();

        [SerializeField] private Image _gameBonus;
        [SerializeField] private Image _bonus;

        [SerializeField] private GameObject _presentBox;
        [SerializeField] private GameObject _takePresentBtn;

        [SerializeField] private Text _timerText;

        private float _timeLeft = DayInSeconds;
        private bool _enableTimer;


        private void Awake()
        {
            if (_playerPrefsController.HasKey("CurrentBonus"))
            {
                _gameBonus.sprite = _gamebonuSpritesList[_playerPrefsController.GetInt("CurrentBonus")];
            }
            else
            {
                _gameBonus.gameObject.SetActive(false);
            }
        }

        private void Start()
        {
            OneSignal.StartInit("1ae6105c-6226-4ded-9195-f0a7805e5c88").EndInit();
        }

        private void Update()
        {
            HourlyBonus();
        }

        private void ReceiveNotification(float seconds)
        {
            var notification = new Dictionary<string, object>();
            notification["contents"] = new Dictionary<string, string>
                {
                    {
                        "en",
                        "Open the game to take your daily reward!"
                    }
                };

            var usersList = new List<string> {OneSignal.GetPermissionSubscriptionState().subscriptionStatus.userId};

            notification["include_player_ids"] = usersList;
            Debug.Log("ID = " + OneSignal.GetPermissionSubscriptionState().subscriptionStatus.userId);
            notification["send_after"] = DateTime.Now.ToUniversalTime().AddSeconds(seconds).ToString("U");
            OneSignal.PostNotification(notification, responseSuccess =>
            {
                Debug.Log("OK");
            }, 
            responseFailure =>
            {
                Debug.Log("NE OK");
                Debug.Log(Json.Serialize(responseFailure));
            });
        }

        private void HourlyBonus()
        {
            if (_enableTimer)
            {
                _timeLeft -= Time.deltaTime;
                TimeSpan timeSpan = TimeSpan.FromSeconds(Mathf.Round(_timeLeft));
                string timeText = string.Format("{0:D2}:{1:D2}:{2:D2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                _timerText.text = timeText;
                if (_timeLeft <= 0)
                {
                    _enableTimer = false;
                    _timerText.gameObject.SetActive(false);
                    _takePresentBtn.SetActive(true);
                }
            }
        }

        private void SetHourlyTimerData()
        {
            if (!_playerPrefsController.HasKey("LastVisit"))
            {
                _playerPrefsController.SetString("LastVisit", "");
                _playerPrefsController.SetFloat("LeftForHourlyBonus", 0f);
                _enableTimer = true;
            }
            else
            {
                float leftForHourlyBonus = _playerPrefsController.GetFloat("LeftForHourlyBonus");
                long dateTimeLong;
                bool didParse = Int64.TryParse(_playerPrefsController.GetString("LastVisit"), out dateTimeLong);

                if (didParse)
                {
                    DateTime lastVisit = DateTime.FromBinary(dateTimeLong);
                    TimeSpan timeDifference = DateTime.Now.Subtract(lastVisit);
                    double timeDifferenceAtSeconds = Math.Round(timeDifference.TotalSeconds);
                    _timeLeft = (float)(leftForHourlyBonus - timeDifferenceAtSeconds);
                    _enableTimer = true;
                }
            }
        }
        private void OnDisable()
        {
            SetLastVisit();
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus)
            {
                if (_playerPrefsController.HasKey("LastVisit"))
                {
                    _timerText.gameObject.SetActive(true);
                    _takePresentBtn.SetActive(false);
                    SetHourlyTimerData();
                }
            }
            else
            {
                SetLastVisit();
            }
        }

        private void SetLastVisit()
        {
            if (_playerPrefsController.HasKey("LastVisit"))
            {
                _playerPrefsController.SetString("LastVisit", DateTime.Now.ToBinary().ToString());
                _playerPrefsController.SetFloat("LeftForHourlyBonus", _timeLeft);
            }
        }

        public void OnTakePresentBtnClick()
        {
            _presentBox.SetActive(true);
            var randomBonusIndex = Random.Range(0, 5);
            _bonus.sprite = _bonuSpritesList[randomBonusIndex];

            _gameBonus.gameObject.SetActive(true);
            _gameBonus.sprite = _gamebonuSpritesList[randomBonusIndex];

            _playerPrefsController.SetInt("CurrentBonus", randomBonusIndex);

            ReceiveNotification(DayInSeconds);
            StartCoroutine(Timer());
        }

        private IEnumerator Timer()
        {
            yield return new WaitForSeconds(1.2f);
            _presentBox.SetActive(false);
            _timerText.gameObject.SetActive(true);

            _playerPrefsController.Delete("LastVisit");
            _playerPrefsController.Delete("LeftForHourlyBonus");

            _timeLeft = DayInSeconds;
            SetHourlyTimerData();
        }

    }

    public enum Bonus
    {
        Score2,
        Health1,
        Freeze,
        Score3,
        Health2
    }
}