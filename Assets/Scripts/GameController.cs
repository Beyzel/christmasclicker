﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Share;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private Transform _itemParent;

        [SerializeField] private GameObject _itemPrefab;
        [SerializeField] private GameObject _restart;
        [SerializeField] private GameObject _menu;
        [SerializeField] private GameObject _tapThePresents;
        [SerializeField] private GameObject _watchAds;

        [SerializeField] private Text _score;
        [SerializeField] private Text _leftHealthText;

        [SerializeField] private List<Sprite> _giftSprites = new List<Sprite>();
        [SerializeField] private BagController _bagController;
        [SerializeField] private Image _bonus;
        [SerializeField] private AdvertisementsController _advertisementsController;
        
        private int _counter;
        private int _giftsCount;
        private int _restartCount;

        private bool _isPlaying;
        private bool _isFirstPlay;
        private bool _isAdsWached;

        private float _delay;
        private float _itemStepMoving;

        private int _scoreFactor;
        private int _healthFactor;
        private float _freezeFactor;

        private readonly PlayerPrefsController _playerPrefsController = new PlayerPrefsController();

        private void Awake()
        {
            InitializeParams();
            InitializeBonus();
        }

        private void InitializeBonus()
        {
            if (_playerPrefsController.HasKey("CurrentBonus"))
            {
                var bonusIndex = _playerPrefsController.GetInt("CurrentBonus");
                switch (bonusIndex)
                {
                    case (int) Bonus.Freeze:
                    {
                        _freezeFactor = 0.75f;
                        break;
                    }

                    case (int) Bonus.Health1:
                    {
                        _healthFactor = 2;
                        break;
                    }

                    case (int) Bonus.Health2:
                    {
                        _healthFactor = 3;
                        break;
                    }

                    case (int) Bonus.Score2:
                    {
                        _scoreFactor = 2;
                        break;
                    }

                    case (int) Bonus.Score3:
                    {
                        _scoreFactor = 3;
                        break;
                    }
                }
            }
        }

        private void InitializeParams()
        {
            _delay = 0.5f;
            _itemStepMoving = 5f;
            _scoreFactor = 1;
            _healthFactor = 1;
            _freezeFactor = 1;
        }

        public void AddRestartCount()
        {
            _restartCount++;
        }

        public void StartGame()
        {
            if (_restartCount != 0 && _restartCount % 5 == 0 && !bool.Parse(_playerPrefsController.GetString("IsAdsBuy")) && Advertisement.IsReady())
            {
                _advertisementsController.ShowAds(true);
            }
            else
            {
                if (!_isFirstPlay)
                {
                    _tapThePresents.SetActive(true);
                    _isFirstPlay = true;
                }
                else
                {
                    _tapThePresents.SetActive(false);
                    _isPlaying = true;
                    StartCoroutine(GenerateItems());
                }    
            }
        }

        private IEnumerator GenerateItems()
        {
             while (_isPlaying)
            {
                var newItem = Instantiate(_itemPrefab);
                var itemController = newItem.GetComponent<ItemController>();
                var newItemImage = newItem.GetComponent<Image>();

                newItem.transform.SetParent(_itemParent);
                newItem.transform.localScale = Vector3.one;

                var randomXPos = Random.Range(0.1f, 0.9f);
                var randomGiftSpriteIndex = Random.Range(0, _giftSprites.Count);

                var itemPos = new Vector3(Camera.main.ViewportToWorldPoint(new Vector3(randomXPos, 0, 0)).x, Camera.main.ViewportToWorldPoint(new Vector3(0, 1.1f, 0)).y, 0);
                newItem.transform.position = itemPos;
                itemController.Step = _itemStepMoving;

                newItemImage.sprite = _giftSprites[randomGiftSpriteIndex];
            
                ChangeСomplexity();
                itemController.StartItemMoving();
                yield return new WaitForSeconds(_delay);
                _giftsCount++;
            }
        }

        private void ChangeСomplexity()
        {
            if (_giftsCount != 0 && _giftsCount % 10 == 0)
            {
                if (_delay > 0.25)
                {
                    _delay -= 0.02f * _freezeFactor;
                }
                _itemStepMoving += 0.25f * _freezeFactor;
            }
        }

        public void UpdateScore(bool isGameOver = false)
        {
            if (!isGameOver)
            {
                _counter += _scoreFactor;
                ChangeScoreText();
            }
            else
            {
                _healthFactor--;
                if (_healthFactor <= 0)
                {
                    if (_counter >= 30 && !_isAdsWached)
                    {
                        StopGame();
                        _watchAds.SetActive(true);
                    }
                    else
                    {
                        _isAdsWached = false;
                        GameOver();
                    }
                }
                else
                {
                    StopGame();
                    _leftHealthText.gameObject.SetActive(true);
                    _leftHealthText.text = "- 1 health";
                    StartCoroutine(LefthealthTextAlfaDisable());
                }
            }
        }

        private IEnumerator LefthealthTextAlfaDisable()
        {
            while (_leftHealthText.color.a > 0)
            {
                _leftHealthText.color = new Color(_leftHealthText.color.r, _leftHealthText.color.g, _leftHealthText.color.b, _leftHealthText.color.a - 0.01f);
                yield return new WaitForSeconds(0.01f);
            }
            if (_leftHealthText.color.a <= 0)
            {
                _leftHealthText.gameObject.SetActive(false);
                _leftHealthText.color = new Color(_leftHealthText.color.r, _leftHealthText.color.g, _leftHealthText.color.b, 1);
                StartGame();
            }
        }

        public void OnPlayAdsBtnClick()
        {
            _advertisementsController.ShowAds();
        }

        public void OnNoThanksBtnClick()
        {
            GameOver();
        }

        private void GameOver()
        {
            StopGame();

            _menu.SetActive(true);
            _bagController.ChangeWindow(Windows.Restart);

            _restart.transform.GetChild(0).GetComponent<Text>().text = "SCORE:  " + _counter + "\nONE MORE TIME?";
            LeaderBoardController.Instance.SetScoreToLeaderboard(_counter);
            _restart.transform.GetChild(1).GetComponent<ShareController>().Score = _counter;

            _counter = 0;
            _giftsCount = 0;
            ChangeScoreText();
            InitializeParams();
            InitializeBonus();
            gameObject.SetActive(false);
        }

        private void StopGame()
        {
            _isPlaying = false;
            StopAllCoroutines();
            ClearItems();
        }


        private void ChangeScoreText()
        {
            _score.text = _counter.ToString();
        }

        private void ClearItems()
        {
            for (int i = 0; i < _itemParent.childCount; i++)
            {
                Destroy(_itemParent.GetChild(i).gameObject);
            }
        }

        public void AddHealthForWatchingAds()
        {
            _healthFactor++;
            _isAdsWached = true;
            _watchAds.SetActive(false);
            StartGame();
        }
    }
}