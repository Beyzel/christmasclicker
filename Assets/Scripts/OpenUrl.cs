﻿using UnityEngine;

namespace Assets.Scripts
{
    public class OpenUrl : MonoBehaviour
    {
        public string Url;

        public void OnUrlBtnClick()
        {
            Application.OpenURL(Url);
        }
    }
}